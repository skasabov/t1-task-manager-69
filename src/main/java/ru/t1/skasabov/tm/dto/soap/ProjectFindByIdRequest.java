package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "id"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindByIdRequest")
public class ProjectFindByIdRequest {

    @NotNull
    @XmlElement(required = true)
    protected String id;

    public ProjectFindByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
