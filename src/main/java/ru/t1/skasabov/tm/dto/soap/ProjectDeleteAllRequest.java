package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "projects"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectDeleteAllRequest")
public class ProjectDeleteAllRequest {

    @NotNull
    protected List<ProjectDto> projects;

    public ProjectDeleteAllRequest(@NotNull final List<ProjectDto> projects) {
        this.projects = projects;
    }

}
