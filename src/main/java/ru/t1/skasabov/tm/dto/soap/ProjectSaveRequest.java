package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "project"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveRequest")
public class ProjectSaveRequest {

    @NotNull
    protected ProjectDto project;

    public ProjectSaveRequest(@NotNull final ProjectDto project) {
        this.project = project;
    }

}
