package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "id"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskExistsByIdRequest")
public class TaskExistsByIdRequest {

    @NotNull
    @XmlElement(required = true)
    protected String id;

    public TaskExistsByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
