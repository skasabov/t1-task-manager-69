package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "project"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindByIdResponse")
public class ProjectFindByIdResponse {

    @Nullable
    @XmlElement(name = "project")
    protected ProjectDto project;

    public ProjectFindByIdResponse(@Nullable final ProjectDto project) {
        this.project = project;
    }

}
