package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "tasks"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindAllResponse")
public class TaskFindAllResponse {

    @NotNull
    @XmlElement(name = "tasks")
    protected List<TaskDto> tasks;

    public TaskFindAllResponse(@NotNull final List<TaskDto> tasks) {
        this.tasks = tasks;
    }

}
