package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.ProjectDto;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "exists"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectExistsByIdResponse")
public class ProjectExistsByIdResponse {

    @NotNull
    @XmlElement(required = true)
    protected Boolean exists;

    public ProjectExistsByIdResponse(@NotNull final Boolean exists) {
        this.exists = exists;
    }

}
