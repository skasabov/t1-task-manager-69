package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "id"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectExistsByIdRequest")
public class ProjectExistsByIdRequest {

    @NotNull
    @XmlElement(required = true)
    protected String id;

    public ProjectExistsByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
