package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "task"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskSaveResponse")
public class TaskSaveResponse {

    @NotNull
    @XmlElement(name = "task")
    protected TaskDto task;

    public TaskSaveResponse(@NotNull final TaskDto task) {
        this.task = task;
    }

}
