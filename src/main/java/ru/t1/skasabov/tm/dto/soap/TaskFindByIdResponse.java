package ru.t1.skasabov.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "", propOrder = {
    "task"
})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindByIdResponse")
public class TaskFindByIdResponse {

    @Nullable
    @XmlElement(name = "task")
    protected TaskDto task;

    public TaskFindByIdResponse(@Nullable final TaskDto task) {
        this.task = task;
    }

}
