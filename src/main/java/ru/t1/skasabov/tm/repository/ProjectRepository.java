package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
