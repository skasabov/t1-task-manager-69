package ru.t1.skasabov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.skasabov.tm.dto.UserDto;

public interface UserDtoRepository extends JpaRepository<UserDto, String> {

    @Nullable
    UserDto findByLogin(@NotNull String login);

}
