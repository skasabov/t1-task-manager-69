package ru.t1.skasabov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class IndexController {

    @NotNull
    @GetMapping("/")
    public String index() {
        return "index";
    }

}
