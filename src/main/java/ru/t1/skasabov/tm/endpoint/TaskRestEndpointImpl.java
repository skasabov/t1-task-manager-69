package ru.t1.skasabov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.skasabov.tm.api.ITaskRestEndpoint;

import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.repository.TaskDtoRepository;
import ru.t1.skasabov.tm.util.UserUtil;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public final class TaskRestEndpointImpl implements ITaskRestEndpoint {

    @NotNull
    @Autowired
    private TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return taskRepository.findAllByUserId(UserUtil.getUserId());
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public TaskDto save(
            @NotNull @RequestBody final TaskDto task
    ) {
        task.setUserId(UserUtil.getUserId());
        return taskRepository.save(task);
    }

    @NotNull
    @Override
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull @PathVariable("id") final String id
    ) {
        return taskRepository.findByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull @PathVariable("id") final String id
    ) {
        return taskRepository.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.countByUserId(UserUtil.getUserId());
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull @PathVariable("id") final String id
    ) {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @NotNull @RequestBody final TaskDto task
    ) {
        taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull @RequestBody final List<TaskDto> tasks
    ) {
        for (@NotNull final TaskDto task : tasks) {
            taskRepository.deleteByUserIdAndId(UserUtil.getUserId(), task.getId());
        }
    }

    @Override
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAllByUserId(UserUtil.getUserId());
    }

}
