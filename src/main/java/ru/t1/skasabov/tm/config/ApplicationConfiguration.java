package ru.t1.skasabov.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;
import ru.t1.skasabov.tm.endpoint.*;

@EnableWs
@Configuration
@ComponentScan("ru.t1.skasabov.tm")
public class ApplicationConfiguration extends WsConfigurerAdapter {

    @NotNull
    @Bean(name = "ProjectEndpoint")
    public DefaultWsdl11Definition projectWsdl11Definition(@NotNull final XsdSchema projectEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(ProjectSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(ProjectSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(ProjectSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(projectEndpointSchema);
        return wsdl11Definition;
    }

    @NotNull
    @Bean(name = "TaskEndpoint")
    public DefaultWsdl11Definition taskWsdl11Definition(@NotNull final XsdSchema taskEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(TaskSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(TaskSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(TaskSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(taskEndpointSchema);
        return wsdl11Definition;
    }

    @NotNull
    @Bean(name = "AuthEndpoint")
    public DefaultWsdl11Definition authWsdl11Definition(@NotNull final XsdSchema authEndpointSchema) {
        @NotNull final DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(AuthSoapEndpointImpl.PORT_TYPE_NAME);
        wsdl11Definition.setLocationUri(AuthSoapEndpointImpl.LOCATION_URI);
        wsdl11Definition.setTargetNamespace(AuthSoapEndpointImpl.NAMESPACE);
        wsdl11Definition.setSchema(authEndpointSchema);
        return wsdl11Definition;
    }

    @Bean
    @NotNull
    public XsdSchema projectEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/projectEndpoint.xsd"));
    }

    @Bean
    @NotNull
    public XsdSchema taskEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/taskEndpoint.xsd"));
    }

    @Bean
    @NotNull
    public XsdSchema authEndpointSchema() {
        return new SimpleXsdSchema(new ClassPathResource("xsd/authEndpoint.xsd"));
    }

    @Bean
    @NotNull
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
