package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.AuthRestEndpointClient;
import ru.t1.skasabov.tm.dto.Result;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

public class AuthRestEndpointClientTest {

    @NotNull
    private final AuthRestEndpointClient client = AuthRestEndpointClient.client();

    private boolean isLogout = true;

    @Before
    public void initTest() {
        @NotNull final Result loginResult = client.login("test", "test");
        Assert.assertTrue(loginResult.isSuccess());
    }

    @After
    public void clean() {
        if (isLogout) {
            @NotNull final Result logoutResult = client.logout();
            Assert.assertFalse(logoutResult.isSuccess());
        }
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testLogin() {
        @NotNull final Result loginResult = client.login("test", "test");
        Assert.assertTrue(loginResult.isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testEmptyLogin() {
        @NotNull final Result loginResult = client.login("", "test");
        Assert.assertFalse(loginResult.isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testLoginEmptyPassword() {
        @NotNull final Result loginResult = client.login("test", "");
        Assert.assertFalse(loginResult.isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testIncorrectLogin() {
        @NotNull final Result loginResult = client.login("admin", "test");
        Assert.assertFalse(loginResult.isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testLoginIncorrectPassword() {
        @NotNull final Result loginResult = client.login("test", "admin");
        Assert.assertFalse(loginResult.isSuccess());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testProfile() {
        @Nullable final UserDto user = client.profile();
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testLogout() {
        isLogout = false;
        @NotNull final Result logoutResult = client.logout();
        Assert.assertFalse(logoutResult.isSuccess());
    }

}
