package ru.t1.skasabov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.skasabov.tm.client.AuthRestEndpointClient;
import ru.t1.skasabov.tm.client.TaskRestEndpointClient;
import ru.t1.skasabov.tm.dto.TaskDto;
import ru.t1.skasabov.tm.dto.UserDto;
import ru.t1.skasabov.tm.marker.IntegrationCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskRestEndpointClientTest {

    private final static int NUMBER_OF_ENTITIES = 4;

    @NotNull
    private String userId;

    @NotNull
    private final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    @NotNull
    private final AuthRestEndpointClient authClient = AuthRestEndpointClient.client();

    @NotNull
    private List<TaskDto> taskList = new ArrayList<>();

    @NotNull
    private final TaskDto task1 = new TaskDto("Test Task 1");

    @NotNull
    private final TaskDto task2 = new TaskDto("Test Task 2");

    @NotNull
    private final TaskDto task3 = new TaskDto("Test Task 3");

    @NotNull
    private final TaskDto task4 = new TaskDto("Test Task 4");

    @Before
    public void initTest() {
        authClient.login("test", "test");
        @Nullable final UserDto user = authClient.profile();
        Assert.assertNotNull(user);
        userId = user.getId();
        task1.setUserId(userId);
        task2.setUserId(userId);
        task3.setUserId(userId);
        task4.setUserId(userId);
        taskList = client.findAll();
        client.clear();
        client.save(task1);
        client.save(task2);
        client.save(task3);
        client.save(task4);
    }

    @After
    public void clean() {
        client.clear();
        for (@NotNull final TaskDto task : taskList) client.save(task);
        authClient.logout();
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindAll() {
        @NotNull final List<TaskDto> tasks = client.findAll();
        Assert.assertEquals(NUMBER_OF_ENTITIES, tasks.size());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testSave() {
        @NotNull final TaskDto task = new TaskDto("Test Task");
        task.setUserId(userId);
        client.save(task);
        Assert.assertEquals(NUMBER_OF_ENTITIES + 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindById() {
        @Nullable final TaskDto Task = client.findById(task1.getId());
        Assert.assertNotNull(Task);
        Assert.assertEquals("Test Task 1", Task.getName());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testFindByInvalidId() {
        @Nullable final TaskDto task = client.findById("123");
        Assert.assertNull(task);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsById() {
        boolean existsTask = client.existsById(task1.getId());
        Assert.assertTrue(existsTask);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testExistsByInvalidId() {
        boolean existsTask = client.existsById("123");
        Assert.assertFalse(existsTask);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testCount() {
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteById() {
        client.deleteById(task1.getId());
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDelete() {
        client.delete(task1);
        Assert.assertEquals(NUMBER_OF_ENTITIES - 1, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteTasks() {
        @NotNull final List<TaskDto> tasks = new ArrayList<>();
        tasks.add(task2);
        tasks.add(task3);
        tasks.add(task4);
        client.deleteAll(tasks);
        Assert.assertEquals(NUMBER_OF_ENTITIES - tasks.size(), client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testDeleteEmptyTasks() {
        client.deleteAll(Collections.emptyList());
        Assert.assertEquals(NUMBER_OF_ENTITIES, client.count());
    }

    @Test
    @Category(IntegrationCategory.class)
    public void testClear() {
        client.clear();
        Assert.assertEquals(0, client.count());
    }
    
}
